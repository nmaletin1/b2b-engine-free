$(function() {
	
  var headerHeight = $('header').outerHeight();
  var windowHeight = $(window).height();

  $(window).on('load resize', function() {
    headerHeight = $('header').outerHeight();
    document.documentElement.style.setProperty('--header-height', headerHeight + 'px');
    windowHeight = $(window).height();
	});  
  
  /* Header Position */
  function headerPosition(headerHeight) {
    if ( $(document).scrollTop() > headerHeight ) {
      $('header:not(.header-static)').addClass('reactive');
    }
    else {
      $('header:not(.header-static)').removeClass('reactive');
    }
  }

  var lastScrolledFromTop = headerHeight;
  
  $(window).on('scroll', function() {
    headerPosition(headerHeight);
    
    var scrolledFromTop = $(this).scrollTop();
    
    if ( scrolledFromTop > lastScrolledFromTop ) {
      /*down scroll*/
      if ($('#progress-wrapper').length) {
        $('.header-reactive').css('margin-top', '-' + ( parseInt(headerHeight) - 5 ) + 'px');
      } else {
        $('.header-reactive').css('margin-top', '-' + parseInt(headerHeight) + 'px');
      }
    } else {
      /*up scroll*/
      $('.header-reactive').css('margin-top', '0');
    }
    
    if ( $(document).scrollTop() > headerHeight ) {
      lastScrolledFromTop = scrolledFromTop;
    }
    
    if ( scrolledFromTop > windowHeight ) {
      $('.footer-scroll-to-top').addClass('footer-scroll-to-top-active');
    } else {
      $('.footer-scroll-to-top').removeClass('footer-scroll-to-top-active');
    }
  });
  

  /* Search and Language Switcher */
  if ( $('.header-language-switcher-wrapper').length ) {
    $('.header-language-switcher').appendTo('.header-language-switcher-wrapper');
  }
  
  if ( $('.header-main-container').hasClass('full-width') ) {
    $('.header-search-inner').addClass('full-width');    
  }
  
  if ( $('.header-search--pop-up').length ) {
    $('.header-search-open').appendTo('.header-search-wrapper');
    $('.header-search-open').removeClass('hidden');
    $('.header-search-close').css('top', $('.header-search-open').offset().top - $(window).scrollTop());
    //$('.header-search-close').css('right', $(window).width() - ($('.header-search-open').offset().left + $('.header-search-open').outerWidth()));
  } else if ( $('.header-search--inline').length ) {
    $('.header-search').appendTo('.header-search-wrapper');
    if ( $('.header-search--slide-in').length ) {
      $('.header-search-open').appendTo('.header-search-wrapper');
      $('.header-search-close').appendTo('.header-search-wrapper');
      $('.header-search-open').removeClass('hidden');
      $('.header-search-close').addClass('hidden');
    }
  }

	$('.header-search-open').on('click', function() {
		$('.header-search').addClass('header-search-active');
    if ( $('.header-search--inline').length ) {
      $('.header-search-open').addClass('hidden');
      $('.header-search-close').removeClass('hidden');
    }
    return false;
	});
  
	$('.header-search-close').on('click', function() {
    $('.header-search').removeClass('header-search-active');
    if ( $('.header-search--inline').length ) {
      $('.header-search-open').removeClass('hidden');
      $('.header-search-close').addClass('hidden');
    }
    return false;
	});
  
  
  /* AOS */
  /*if ( !$('html').hasClass('hs-inline-edit') ) {*/
  AOS.init({
    // Global settings:
    disable:
      {% if request.postDict.inpageEditorUI %}
        true
      {% elif theme.animations.enable_animations and theme.animations.disable_animations == "0" %}
        false
      {% elif theme.animations.enable_animations and theme.animations.disable_animations != "0" %}
        function() {
          return window.innerWidth < {{ theme.animations.disable_animations }}; 
        }
      {% else %}
        true
      {% endif %}
    , // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
    startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
    initClassName: 'aos-init', // class applied after initialization
    animatedClassName: 'aos-animate', // class applied on animation
    useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
    disableMutationObserver: false, // disables automatic mutations' detections (advanced)
    debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
    throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)
    // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
    offset: 0, // offset (in px) from the original trigger point
    delay: {{ theme.animations.animation_delay }}, // values from 0 to 3000, with step 50ms
    duration: {{ theme.animations.animation_duration }}, // values from 0 to 3000, with step 50ms
    easing: 'ease', // default easing for AOS animations
    once: true, // whether animation should happen only once - while scrolling down
    mirror: false, // whether elements should animate out while scrolling past them
    anchorPlacement: 'center', // defines which position of the element regarding to window should trigger the animation
  });
  /*};*/
  
  
  /* Scroll to Top */
	$('.footer-scroll-to-top').on('click', function() {
		$('html, body').animate({ scrollTop: 0 }, 'slow');
		return false;
	});
  
  
  /* Page Slug as Body Class */
  function addPageSlugBodyClass() {
    let pathArray = window.location.pathname.split('/');
    let pageSlug = pathArray.pop();
    if ( pageSlug == '' ) {
      pageSlug = 'home';
    }
    let pageSlugClass= 'page-' + pageSlug;
    $('body').addClass(pageSlugClass);
  };
  
  addPageSlugBodyClass();
  
  
  /* Scroll to Page Section */
	$('a[href*="#"]').not('[href="#"]').on('click', function(event) {
    if ( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname ) {
      var target = $(this.hash);
      if ( target.length ) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 500, function() {
          var $target = $(target);
          $target.focus();
          if ( $target.is(':focus') ) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
	});
  
  
	/* Mobile Menu */   
	$('.header-menu-mobile .hs-item-has-children > a').after('<div class="submenu-button">{% icon name="chevron-down" style="SOLID" unicode="f078", no_wrapper=True %}</div>');
	$('.menu-mobile-button').click(function() {
		$('.hs-menu-children-wrapper').hide();
    $('.header-menu-mobile').slideToggle(200);
		return false;
	});

	$('.submenu-button').click(function() {
		$(this).parent().siblings('.hs-item-has-children').find('.hs-menu-children-wrapper').slideUp(200);
		$(this).next('.hs-menu-children-wrapper').slideToggle(200);
		$(this).next('.hs-menu-children-wrapper').children('.hs-item-has-children').find('.hs-menu-children-wrapper').slideUp(200);
		return false;
	});
  

  /* Slick Slider */ 
  $('.slider-enabled').each(function() {
    var arrows = $(this).data('slider-arrows');
    var dots = $(this).data('slider-dots');
    var itemsInRow = $(this).data('slider-items');

    if ( itemsInRow == 1 ) {
      $(this).slick({
        arrows: arrows,
        dots: dots,
        slidesToShow: itemsInRow,
        slidesToScroll: 1,
        infinite: true,
        autoplay: false,
        autoplaySpeed: 4000,
        adaptiveHeight: false,
        responsive: [
          {
            breakpoint: {{ theme.spacing.max_width + 120 }},
            settings: {
              arrows: false
            }
          }
        ]
      });
    }
    else {
      $(this).slick({
        arrows: arrows,
        dots: dots,
        slidesToShow: itemsInRow,
        slidesToScroll: 1,
        infinite: true,
        autoplay: false,
        autoplaySpeed: 4000,
        adaptiveHeight: false,
        responsive: [
          {
            breakpoint: {{ theme.spacing.max_width + 120 }},
            settings: {
              arrows: false
            }
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
              arrows: false
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              arrows: false
            }
          }
        ]
      });
    }
  });
  
  
  /* Magnific Popup */
  $('.popup-enabled').magnificPopup({
    mainClass: 'mfp-with-zoom',
    fixedContentPos: true,
    fixedBgPos: true,
    overflowY: 'auto',
    closeBtnInside: true,
    preloader: false,
    midClick: true,
    removalDelay: 300,
    closeMarkup: '<div title="%title%" class="mfp-close">{% icon name="times" style="SOLID" unicode="f00d", no_wrapper=True %}</div>',
    callbacks: {
      open: function() {
        $('body').addClass('popup-open')
      },
      close: function() {
        $('body').removeClass('popup-open')
      }
    },
    type: 'inline'
  });

  /* Magnific Popup Image */
  $('.popup-image-enabled').magnificPopup({
    mainClass: 'mfp-with-zoom',
    fixedContentPos: true,
    fixedBgPos: true,
    overflowY: 'auto',
    closeBtnInside: true,
    preloader: false,
    midClick: true,
    removalDelay: 300,
    closeMarkup: '<div title="%title%" class="mfp-close">{% icon name="times" style="SOLID" unicode="f00d", no_wrapper=True %}</div>',
    callbacks: {
      open: function() {
        $('body').addClass('popup-open')
      },
      close: function() {
        $('body').removeClass('popup-open')
      }
    },
    type: 'image'
  });

  /* Magnific Popup Gallery */
  $('.popup-gallery-enabled').each(function() { // the containers for all your galleries
      $(this).magnificPopup({
        mainClass: 'mfp-with-zoom',
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        closeMarkup: '<div title="%title%" class="mfp-close">{% icon name="times" style="SOLID" unicode="f00d", no_wrapper=True %}</div>',
        callbacks: {
          open: function() {
            $('body').addClass('popup-open')
          },
          close: function() {
            $('body').removeClass('popup-open')
          }
        },
        type: 'image',
        delegate: '.popup-gallery-item', // the selector for gallery item
        gallery: {
          enabled: true,
          preload: [1,2], // 1 previous, 2 next
          navigateByImgClick: false,
          arrowMarkup: '<div title="%title%" class="mfp-arrow mfp-arrow-%dir%"></div>', // markup of an arrow button
          tPrev: 'Previous (Left arrow key)', // title for left button
          tNext: 'Next (Right arrow key)', // title for right button
          tCounter: '<span class="mfp-counter">%curr% of %total%</span>' // markup of counter
        }
      });
  });


  /* Blog Reading Progress Bar */
  function setProgress(headerHeight) {
    var start = document.querySelector('.blog-post').getBoundingClientRect().top;
    var height = document.querySelector('.blog-post').getBoundingClientRect().height;
    if ( start > 0 ) {
      $('#progress-bar').css('width', 0);
    } else {
      $('#progress-bar').css('width', Math.abs(start) / height * 100 + "%");
    }
    if ( $('.header-sticky').length ) {
      $('#progress-wrapper').css('top', headerHeight);
    } else if ( $('.header-reactive').length ) {
      $('header').append($('#progress-wrapper'));
    }
  }

  if ( $('.blog-post').length ) {
    $(window).on('scroll', function() {
      setProgress(headerHeight);
    });
  }


  /* Blog Sidebar Search */
  if ( $('.blog-index__sidebar-search').length ) {
    $('.blog-index__sidebar-search-inner').appendTo('.blog-index__sidebar-search');
  }


  /* Section Options */
  $('.section-options').closest('.row-fluid-wrapper').addClass('section-options-wrapper');
  

  /*Accordion Auto Toggle*/
  $('.accordion-auto-toggle .accordion-head').on('click', function() {
    if ( $(this).closest('.accordion-single').hasClass('active') ) {
      $(this).closest('.accordion-single').removeClass('active');
      $(this).siblings('.accordion-body').slideUp();
    } else {
      $(this).closest('.accordion').find('.accordion-single').removeClass('active');
      $(this).closest('.accordion-single').addClass('active');
      $(this).closest('.accordion').find('.accordion-single:not(.active) .accordion-body').slideUp();
      $(this).siblings('.accordion-body').slideDown();
    }
  });

  /*Accordion Manual Toggle*/
  $('.accordion-manual-toggle .accordion-head').on('click', function() {
    $(this).closest('.accordion-single').toggleClass('active');
    $(this).siblings('.accordion-body').slideToggle();
  });


  /* Tabs */
  $('.tab-button').on('click', function() {
    if ( !$(this).hasClass('active') ) {
      $(this).closest('.tab-list').find('.tab-button').removeClass('active');
      $(this).addClass('active');

      var activeTab = $(this).data('tab');
      $(this).closest('.tabs').find('.tab-body-inner').not('#' + activeTab).hide();
      $('#' + activeTab).fadeIn();
      return false;
    }
  });

  /* Open First Tab on Page Load */
  $('.tab-button:first-child').addClass('active');
  $('.tab-body-inner:first-child').show();


  /* Parallax Background */
  var parallaxLevel = -0.25; /* Change value to adjust parallax level */
  var parallaxBackgrounds = $('.bg-parallax');
  var parallaxElementOffsetTops = [];

  parallaxBackgrounds.each(function(i, el) {
    parallaxElementOffsetTops.push($(el).offset().top);
  });

  $(window).on('resize', function() {
    if ($(window).width() > 767) {
      parallaxElementOffsetTops = [];
      parallaxBackgrounds.each(function(i, el) {
        parallaxElementOffsetTops.push($(el).offset().top);
      });
    } else {
      parallaxBackgrounds.each(function(i, el) {
        $(el).removeAttr('style');
      });
    }
  });

  $(window).on('scroll', function() {
    if ($(window).width() > 767) {
      var scrollTop = $(this).scrollTop();
      parallaxBackgrounds.each(function(i, el) {
        var offsetTop = parallaxElementOffsetTops[i];
        $(el).attr('style', 'background-position-y: ' + (scrollTop - offsetTop) * parallaxLevel + 'px !important');
      });
    } else {
      parallaxBackgrounds.each(function(i, el) {
        $(el).removeAttr('style');
      });
    }
  });
});


/* Number Counter */
function numberCounter(el) {
  var $this = $(el).find('.counter-number-inner');
  $({ Counter: 0 }).animate({ 
    Counter: $this.attr('data-count') 
  },
  {
    duration: 2000,
    easing: 'linear',
    step: function () {
      $this.text(Math.floor(this.Counter));
    },
    complete: function() {
      $this.text(this.Counter);
    }
  });
};


/* Element Scrolled into View */
function isScrolledIntoView(el){
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();
  var elTop = $(el).offset().top;
  var elBottom = elTop + $(el).height();
  return ((elBottom <= docViewBottom) && (elTop >= docViewTop));
}